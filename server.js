var express = require('express');
var graphqlHTTP = require('express-graphql');
var {buildSchema} = require('graphql');
:

var schema = buildSchema(`
    type Query {
        title: String
        writer: String
        publishTime: String
        article: String
        commentCount: Int!
    }
`);

var root = {
    title : () => {
        return 'Inilah 10 alasan Anda tidak perlu tidur, nomor 3 pasti buat Anda tercengang!';
    },
    writer : () => {
        return 'Sumijan'
    },
    publishTime : () => {
        return '20-07-2019 13:00'
    },
    article : () => {
        return 'blablablablabla'
    },
    commentCount : () => {
        return 3;
    }
};

var app = express();
app.use('/graphql', graphqlHTTP({
    schema : schema,
    rootValue: root,
    graphiql: true,
}));

app.listen(4000);
console.log('Running on port 4000');
