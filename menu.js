var express = require('express');
var graphqlHTTP = require('express-graphql');
var {buildSchema} = require('graphql');

const dataMakanan =[{
    nama: 'Pizza',
    harga: 10000,
    level: 1
}]

const dataMinuman = [{
    nama: 'Soda',
    harga: 10000,
    size: 'S'
}]

var schema = buildSchema(`
    type Makanan {
        nama: String
        harga: Int
        level: Int
    }

    type Minuman {
        nama: String
        harga: Int
        size: String
    }

    type Query{
        makanan: [Makanan]
        minuman: [Minuman]
    }
`);

let root = {
    makanan: () => dataMakanan,
    minuman: () => dataMinuman
}

var app = express();
app.use('/', graphqlHTTP({
    schema : schema,
    rootValue: root,
    graphiql: true,
}));

app.listen(3000);
console.log('Running on port 3000');